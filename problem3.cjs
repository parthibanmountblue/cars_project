function problem_3(inventory)
{
    mysort = inventory.sort((a, b) => {
        let fa = a.car_make.toLowerCase(),
            fb = b.car_make.toLowerCase();
    
        if (fa < fb) {
            return -1;
        }
        if (fa > fb) {
            return 1;
        }
        return 0;
    });
    console.log(mysort);
}

module.exports = problem_3;
